import React,{useState} from 'react';

const usePais = (stateInicial,paises) => {
    //state del custom
    const [pais, actualizarPais] = useState(stateInicial);

    const SelectPais= ()=>(
        <select
            className="browser-default"
            onChange={e=>actualizarPais(e.target.value)}
            value={pais}
        >
            {paises.map(pais=>(
                <option key={pais.value} value={pais.value}>{pais.label}</option>
            ))}
        </select>
    );

    return [pais, SelectPais];
}
 
export default usePais;