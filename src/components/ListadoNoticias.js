import React from 'react';
import Noticia from './Noticias';
import PropTypes from 'prop-types';

const ListadoNoticias = ({noticias}) => {

    if(!noticias)return null;
    return ( 
        <div className="row">
            {noticias.map(noticia=>(
            <Noticia
                key={noticia.url}
                noticia={noticia}
            />
        ))}
        </div>
      );
}
ListadoNoticias.propTypes={
    noticias: PropTypes.array.isRequired
}
export default ListadoNoticias;