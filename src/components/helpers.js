export  const Fondo = (source)=>{
    let clase;
    if (source.name==='Infobae'){
        clase='Infobae';
    }
    else if (source.name==='La Voz del Interior'){
        clase='La-Voz-del-Interior';
    }
    else if (source.name==='Clarín'){
        clase='Clarin';
    }
    else if (source.name==='La Nacion'){
        clase='La-Nacion';
    }
    else if (source.name==='Los Andes (Mendoza)'){
        clase='Los-Andes-Mendoza';
    }
    else if (source.name==='Investing.com'){
        clase='Investing';
    }
    else if (source.name==='MDZ Online'){
        clase='MDZ-Online';
    }
    else if (source.name==='Diario El Dia. www.eldia.com'){
        clase='Diario-El-Dia';
    }
    else if (source.name==='El Litoral'){
        clase='El-Litoral';
    }
    else if (source.name==='CNN'){
        clase='CNN';
    }
    else if (source.name==='Mundodeportivo.com'){
        clase='Mundodeportivo';
    }
    else if (source.name==='Perfil.com'){
        clase='Perfil';
    }
    else if (source.name==='Olé'){
        clase='Ole';
    }

    return clase;
}

