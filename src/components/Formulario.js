import React from 'react';
import styles from './Formulario.module.css';
import useSelect from '../hooks/useSelect';
import usePais from '../hooks/usePais';
import PropTypes from 'prop-types';


const Formulario = ({guardarCategoria,guardarPais}) => {

    const OPCIONES=[
        {value:'general', label:'General'},
        {value:'business', label:'Negocios'},
        {value:'entertainment', label:'Entretenimiento'},
        {value:'health', label:'Salud'},
        {value:'science', label:'Ciencia'},
        {value:'sports', label:'Deportes'},
        {value:'technology', label:'Tecnología'}
    ]
    const OPCIONESP=[
        {value:'ar', label:'Argentina'},
        {value:'mx', label:'México'},
        {value:'ve', label:'Venezuela'},
        {value:'il', label:'Israel'},
        {value:'co', label:'Colombia'},
        {value:'pt', label:'Portugal'},
        {value:'br', label:'Brasil'}
    ]

    
    //custom hook de pais
    const [pais, SelectPais]=usePais('ar',OPCIONESP);
    
    //utilizar custom hook useSelect
    const [categoria, SelectNoticias]= useSelect('general',OPCIONES);

    
    //submit al form, pasar categoria a app.js
    const buscarNoticias=e=>{
        e.preventDefault();

        guardarPais(pais);
        guardarCategoria(categoria);
    }
    
    return ( 
<div className={`${styles.buscador} row`}>
    <div className="col s12 m8 offset-m2">
        <form
            onSubmit={buscarNoticias}
        >
            <h2 className={styles.heading}>Encuentra Noticias por Categoría</h2>

            <SelectPais/>
            <SelectNoticias/>

            <div className="input-field col s12">
                <input
                    type="submit"
                    className={`${styles['btn-block']} btn-large amber darken-2`}
                    value="Buscar"
                />
            </div>
        </form>
    </div>
</div>

    );
}
Formulario.propTypes={
    guardarCategoria: PropTypes.func.isRequired
}
 
export default Formulario;