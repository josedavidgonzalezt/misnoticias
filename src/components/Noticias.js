import React from 'react';
import styles from './Formulario.module.css';
import {Fondo} from './helpers'
import PropTypes from 'prop-types';


const Noticia = ({noticia}) => {
  
    //extraer los datos
    const {urlToImage, url, title, description, source}= noticia;

 

    //filtramos si trae imagen o no
    const imagen= (urlToImage) 
        ? 
            <div className="card-image">
                <img src={urlToImage} alt={title}/>
                <span className="card-title fondo" id={Fondo(source)}>{source.name}</span>
            </div>
        :null;
    const bajada = (description)
        ? <p>{description.slice(0,43)}...</p>
        :null;
        
    return (  
        <div className={`${styles.minoticia} col s12 m6 l4`} >
            <div className="card">
                {imagen}
                <div className="card-content">
                    <h5>{title.slice(0,50)}...</h5>
                    {bajada}
                </div>
                <div className="card-action">
                    <a
                        href={url}
                        target="_blank"
                        rel="noopener noreferrer"
                        className="waves-effect waves-ligth btn"
                    >Ver Noticia</a>
                </div>
            </div>
        </div>
    );
}
Noticia.propTypes={
    noticia: PropTypes.object.isRequired
}

 
export default Noticia;