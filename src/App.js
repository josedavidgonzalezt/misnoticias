import React, {Fragment,useState,useEffect} from 'react';
import Header from './components/Header';
import Formulario from './components/Formulario';
import ListadoNoticias from './components/ListadoNoticias';

function App() {
//definir categoria y noticias
const[categoria,guardarCategoria]=useState('general');
const [pais,guardarPais]=useState('ar');
const [noticias, guardarNoticias]=useState([]);

useEffect(() => {
  const consultarAPI= async()=>{
    const url = `https://newsapi.org/v2/top-headlines?country=${pais}&category=${categoria}&apiKey=5edb34f3adbf41d2abc868978119a7c7`;

    const respuesta = await fetch(url);
    const noticias = await respuesta.json();

    guardarNoticias(noticias.articles);
      
  }
  consultarAPI();
}, [categoria, pais]);



  return (
    
    <Fragment>
      <Header
        titulo='Buscador de Noticias'
      />
      <div className="container white">
        <Formulario
          guardarCategoria={guardarCategoria}
          guardarPais={guardarPais}
        />
        <ListadoNoticias
          noticias={noticias}
        />
      </div>
    </Fragment>

  );
}

export default App;
